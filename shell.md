# Cloud Shell


https://console.cloud.google.com

## Cloud Shell

``` sh
$ gcloud cloud-shell ssh
```

VSCode Remote:

```sh
$ gcloud alpha cloud-shell ssh --dry-run
$ nano .ssh/config # update IP
```

In .ssh/config:
```sh 
Host google
        User david
        HostName 34.140.161.34
        Port 6000
        ForwardAgent yes
```
